package np.com.naxa.bettersync.model.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import np.com.naxa.bettersync.BetterSync;
import np.com.naxa.bettersync.model.rest.Food;


public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "bettersync.db";

    public static final String TABLE_FOOD = "food";

    private static final String KEY_ID = "food_id";
    private static final String KEY_NAME = "food_name";
    private static String KEY_LAST_MODIFIED_DATE_TIME = "last_modified_data_time";

    private static DatabaseHelper databaseHelper;


    public static DatabaseHelper getInstance() {


        if (databaseHelper != null) {
            return databaseHelper;
        }

        databaseHelper = new DatabaseHelper(BetterSync.getAppContext());
        return databaseHelper;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_FOOD + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_LAST_MODIFIED_DATE_TIME + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);
        onCreate(db);
    }


    public String getLastSyncDate(String tableName) {


        SQLiteDatabase db = this.getWritableDatabase();
        String datetime;
        String minimumDateTime = "0";

        Cursor cursor = db.query(tableName, new String[]{KEY_LAST_MODIFIED_DATE_TIME}
                , null, null, null, null, KEY_LAST_MODIFIED_DATE_TIME + " DESC ", "1");


        try {
            cursor.moveToFirst();
            datetime = cursor.getString(cursor.getColumnIndex(KEY_LAST_MODIFIED_DATE_TIME));
        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {
            datetime = minimumDateTime;
        } finally {
            cursor.close();
            db.close();
        }

        return datetime;
    }


    public void addFoodItems(List<Food> foodlist) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (Food food : foodlist) {

            values.put(KEY_ID, food.getFoodId());
            values.put(KEY_NAME, food.getFoodName());
            values.put(KEY_LAST_MODIFIED_DATE_TIME, food.getLastModifiedDateTime());

            db.replace(TABLE_FOOD, null, values);
        }


        db.close();
    }

    public List<Food> getFoodList() {


        SQLiteDatabase db = this.getWritableDatabase();
        List<Food> foodList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_FOOD, null
                , null, null, null, null, null, null);
        try {

            cursor.moveToFirst();
            do {
                Food food = new Food();
                food.setFoodId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                food.setFoodName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                food.setLastModifiedDateTime(cursor.getString(cursor.getColumnIndex(KEY_LAST_MODIFIED_DATE_TIME)));
                foodList.add(food);

            }
            while (cursor.moveToNext());

        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {


        } finally {
            cursor.close();
            db.close();
        }

        return foodList;
    }


    public void clearTable(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, null, null);
        db.close();
    }
}
