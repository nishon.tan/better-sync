package np.com.naxa.bettersync;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

/**
 * Created by nishon on 7/23/17.
 */

public class BetterSync extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public void onCreate() {
        super.onCreate();
        BetterSync.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return BetterSync.context;
    }
}
