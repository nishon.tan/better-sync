package np.com.naxa.bettersync.foodlist;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import np.com.naxa.bettersync.R;
import np.com.naxa.bettersync.model.local.DatabaseHelper;
import np.com.naxa.bettersync.model.rest.ApiClient;
import np.com.naxa.bettersync.model.rest.ApiInterface;
import np.com.naxa.bettersync.model.rest.Data;
import np.com.naxa.bettersync.model.rest.Food;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static np.com.naxa.bettersync.model.local.DatabaseHelper.TABLE_FOOD;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private TextView tvMenuList;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String lastSyncDate = DatabaseHelper.getInstance().getLastSyncDate(TABLE_FOOD);
        fetchMenuFromServer(lastSyncDate);

    }


    @Override
    protected void onResume() {
        super.onResume();
        tvMenuList = (TextView) findViewById(R.id.tv_food_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_food_list);
        swipeRefreshLayout.setOnRefreshListener(this);

        tryToBuildMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRefresh() {
        fetchMenuFromServer(DatabaseHelper.getInstance().getLastSyncDate(TABLE_FOOD));
    }

    private void fetchMenuFromServer(String lastSyncDateTime) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Data> call = apiService.getMenu(lastSyncDateTime);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                handleResponse(response);
            }

            private void handleResponse(Response<Data> response) {
                if (response.code() != 200) {
                    showToast("Something went wrong");
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }

                Data data = response.body();
                DatabaseHelper.getInstance().addFoodItems(data.getData());
                tryToBuildMenu();

                swipeRefreshLayout.setRefreshing(false);
                showToast(data.getData().size() + " new food items received");
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showToast(t.getMessage());
            }
        });

    }

    private void tryToBuildMenu() {
        List<Food> foodList = DatabaseHelper.getInstance().getFoodList();
        if (foodList == null || foodList.size() == 0) {
            setMenu("");
            showToast("Please refresh to get menu");
            return;
        }

        String menu = buildMenu(foodList);
        setMenu(menu);
    }

    private String buildMenu(List<Food> foodList) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Food food : foodList) {
            stringBuilder.append(food.getFoodId());
            stringBuilder.append("\t");
            stringBuilder.append(food.getFoodName());
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();

    }

    private void setMenu(String s) {
        tvMenuList.setText(s);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_clear_menu:
                DatabaseHelper.getInstance().clearTable(TABLE_FOOD);
                tryToBuildMenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}

